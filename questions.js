//welke van deze doet ie en waarom dubbel?

chatApp.replaceHTMLPlaceholders = function(html, languageObject){
	for (var key in languageObject) {
		var re = new RegExp('{{'+key+'}}', 'g');
		html = html.replace(re, languageObject[key]);
	}
	return html;
}

chatApp.setChatWindowText = function() {
	var languageConfig = chatApp.languages[chatApp.currentChat.language];
	/* set chat window title */
	if (typeof(chatApp.currentChat.chatWindowTitleText) === "string"){
		chatApp.chatWindowTitleText.innerHTML = chatApp.currentChat.chatWindowTitleText;
	} else {
		chatApp.chatWindowTitleText.innerHTML = languageConfig.chatWindowTitleText;
	}
}
