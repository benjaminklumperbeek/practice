var gulp = require('gulp'),
    watch = require('./gulp/watch'),
    templates = require('./gulp/templates')
// var {series }= require('gulp')

gulp.task('templates', function(done){
  console.log('now watching templates')
  gulp.watch(['./templates/*.hbs'], templates)
  done()
})

exports.default = gulp.series(templates, watch)
// exports.new = newGulp