Handlebars.registerPartial("dog", Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.isCorrect : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <span class=\"result-good\">\n    "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.correctInd : stack1), depth0))
    + "\n  </span>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <span class=\"result-bad\">\n    "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.incorrectInd : stack1), depth0))
    + "\n  </span>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<div class=\"mdl-card mdl-shadow--2dp dog-card\" data-dog-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n  <div class=\"mdl-card__title mdl-card--expand\"\n    style=\"background: url('images/"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "') center 15% no-repeat #46B6AC;\">\n    <h2 class=\"mdl-card__title-text\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h2>\n  </div>\n  <div class=\"mdl-card__actions mdl-card--border\">\n    <button class=\"mdl-button mdl-button--raised dog-button "
    + alias4((helpers.isChosen || (depth0 && depth0.isChosen) || alias2).call(alias1,"dog",{"name":"isChosen","hash":{},"data":data}))
    + "\">\n      "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.yep : stack1), depth0))
    + "\n    </button>\n    <button class=\"mdl-button mdl-button--raised not-dog-button "
    + alias4((helpers.isChosen || (depth0 && depth0.isChosen) || alias2).call(alias1,"not dog",{"name":"isChosen","hash":{},"data":data}))
    + "\">\n      "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.nope : stack1), depth0))
    + "\n    </button>\n  </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.chosen : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</div>";
},"useData":true}));
this["App"] = this["App"] || {};
this["App"]["templates"] = this["App"]["templates"] || {};
this["App"]["templates"]["dogs"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = container.invokePartial(partials.dog,depth0,{"name":"dog","hash":{"language":((stack1 = (data && data.root)) && stack1.language)},"data":data,"indent":"      ","helpers":helpers,"partials":partials,"decorators":container.decorators})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "      "
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.noDogsMessage : stack1), depth0))
    + "\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.dogs : depth0),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "");
},"usePartial":true,"useData":true});
this["App"]["templates"]["index"] = Handlebars.template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"mdl-layout__header mdl-layout__header--waterfall\">\n  <div class=\"mdl-layout__header-row\">\n    <a class=\"site-title\">\n      <span class=\"mdl-layout-title\">\n        "
    + alias4(((helper = (helper = helpers.siteTitle || (depth0 != null ? depth0.siteTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"siteTitle","hash":{},"data":data}) : helper)))
    + "\n      </span>\n    </a>\n    <!-- Add spacer, to align navigation to the right in desktop -->\n    <div class=\"mdl-layout-spacer\"></div>\n    <!-- Navigation -->\n    <div>\n      <nav class=\"mdl-navigation\">\n        <a class=\"mdl-navigation__link mdl-typography--text-uppercase\"\n          href=\"?filter=dogs"
    + alias4((helpers.getLanguageFilter || (depth0 && depth0.getLanguageFilter) || alias2).call(alias1,(depth0 != null ? depth0.langId : depth0),{"name":"getLanguageFilter","hash":{},"data":data}))
    + "\">"
    + alias4(((helper = (helper = helpers.dogsFilter || (depth0 != null ? depth0.dogsFilter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dogsFilter","hash":{},"data":data}) : helper)))
    + "</a>\n        <a class=\"mdl-navigation__link mdl-typography--text-uppercase\"\n          href=\"?filter=not_dogs"
    + alias4((helpers.getLanguageFilter || (depth0 && depth0.getLanguageFilter) || alias2).call(alias1,(depth0 != null ? depth0.langId : depth0),{"name":"getLanguageFilter","hash":{},"data":data}))
    + "\">"
    + alias4(((helper = (helper = helpers.notDogsfilter || (depth0 != null ? depth0.notDogsfilter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"notDogsfilter","hash":{},"data":data}) : helper)))
    + "</a>\n        <a class=\"mdl-navigation__link mdl-typography--text-uppercase\"\n          href=\"?filter=maybe_dogs"
    + alias4((helpers.getLanguageFilter || (depth0 && depth0.getLanguageFilter) || alias2).call(alias1,(depth0 != null ? depth0.langId : depth0),{"name":"getLanguageFilter","hash":{},"data":data}))
    + "\">"
    + alias4(((helper = (helper = helpers.incomplete || (depth0 != null ? depth0.incomplete : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"incomplete","hash":{},"data":data}) : helper)))
    + "</a>\n      </nav>\n    </div>\n  </div>\n</div>\n<div id=\"score\">\n\n</div>\n<div id=\"languageSwitch\">\n  <button class=\"mdl-button\">\n    "
    + alias4(((helper = (helper = helpers.languageFilter || (depth0 != null ? depth0.languageFilter : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"languageFilter","hash":{},"data":data}) : helper)))
    + "\n  </button>\n</div>\n<div class=\"mdl-layout__content\">\n  <a name=\"top\"></a>\n  <div class=\"mdl-typography--text-center\">\n    <h1>"
    + alias4(((helper = (helper = helpers.siteTitle || (depth0 != null ? depth0.siteTitle : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"siteTitle","hash":{},"data":data}) : helper)))
    + "</h1>\n  </div>\n  <div id=\"theDogs\">\n  </div>\n  <div id=\"pagination\">\n\n  </div>\n  <footer class=\"mdl-mega-footer\">\n    <div class=\"mdl-mega-footer--top-section\">\n      <p class=\"mdl-typography--font-light\">© 2015 Ryan Lewis</p>\n    </div>\n  </footer>\n</div>";
},"useData":true});
this["App"]["templates"]["page"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "  <li><a href=\""
    + alias4(((helper = (helper = helpers.link || (depth0 != null ? depth0.link : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"link","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.number || (depth0 != null ? depth0.number : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"number","hash":{},"data":data}) : helper)))
    + "</a></li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : (container.nullContext || {});

  return "<ul>\n"
    + ((stack1 = helpers.each.call(alias1,(helpers.generatePages || (depth0 && depth0.generatePages) || helpers.helperMissing).call(alias1,(depth0 != null ? depth0.dogs : depth0),{"name":"generatePages","hash":{},"data":data}),{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</ul>";
},"useData":true});
this["App"]["templates"]["score"] = Handlebars.template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : (container.nullContext || {}), alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "  "
    + alias4(((helper = (helper = helpers.correct || (depth0 != null ? depth0.correct : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"correct","hash":{},"data":data}) : helper)))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.correct : stack1), depth0))
    + " |\n  "
    + alias4(((helper = (helper = helpers.incorrect || (depth0 != null ? depth0.incorrect : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"incorrect","hash":{},"data":data}) : helper)))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.incorrect : stack1), depth0))
    + " |\n  "
    + alias4(((helper = (helper = helpers.incomplete || (depth0 != null ? depth0.incomplete : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"incomplete","hash":{},"data":data}) : helper)))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.language : depth0)) != null ? stack1.incomplete : stack1), depth0))
    + " |\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "<button class=\"mdl-button\">\n"
    + ((stack1 = (helpers.generateScore || (depth0 && depth0.generateScore) || helpers.helperMissing).call(depth0 != null ? depth0 : (container.nullContext || {}),(depth0 != null ? depth0.dogs : depth0),{"name":"generateScore","hash":{"language":(depth0 != null ? depth0.language : depth0)},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  <small>Reset?</small>\n</button>";
},"useData":true});